# frozen_string_literal: true

require 'newsriver/api/client'
require 'newsriver/api/error'
require 'newsriver/api/search'