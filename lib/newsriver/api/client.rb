# frozen_string_literal: true

require 'net/http'
require 'oj'

module Newsriver
  module Api
    class Client
      BASE_URL = 'https://api.newsriver.io/v2'

      def initialize(api_key:)
        self.api_key = api_key
      end

      def get(endpoint, options = {})
        uri = build_uri(endpoint: endpoint, query: options.fetch(:query, nil))

        puts format('[GET] %<uri>s', uri: uri)

        response = get_request(uri)
        handle_request_error(response) if response.is_a?(Net::HTTPError)

        Oj.load(response.body, mode: :strict, symbol_keys: true)
      end

      private

      attr_accessor :api_key

      def build_uri(endpoint:, query: nil)
        uri = URI(BASE_URL + endpoint)
        uri.query = URI.encode_www_form(query)

        uri
      end

      def build_headers(headers = {})
        { 'Authorization': api_key }.merge(headers)
      end

      def get_request(uri)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = uri.scheme == 'https'

        request = Net::HTTP::Get.new(uri, build_headers)
        http.request(request)
      end

      def handle_request_error(response)
        code = response.code.to_i

        raise Error.new(status: code, message: 'Rate limit reached') if code == 429
        raise Error.new(status: code, message: 'Client error occurred') if (400...500).cover?(code)
        raise Error.new(status: code, message: 'Server error occurred') if (500...600).cover?(code)
      end
    end
  end
end
