# frozen_string_literal: true

module Newsriver
  module Api
    class Search
      ENDPOINT = '/search'

      def initialize(client:, query:)
        self.client = client
        self.query = query
      end

      def call
        client.get(ENDPOINT, query: default_query.merge(query))
      end

      private

      def default_query
        { query: '', limit: 100, sortBy: :_score, sortOrder: :DESC }
      end

      attr_accessor :client, :query
    end
  end
end