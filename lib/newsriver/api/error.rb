# frozen_string_literal: true

module Newsriver
  module Api
    class Error < StandardError

      attr_accessor :status, :message

      def initialize(status:, message:)
        self.status = status
        self.message = message

        super format('Error [%<status>s] %<message>s', status: status, message: message)
      end
    end
  end
end