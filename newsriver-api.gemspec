# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'newsriver/api/version'

Gem::Specification.new do |spec|
  spec.name          = 'newsriver-api'
  spec.version       = Newsriver::Api::VERSION
  spec.authors       = ['Vincent Robbemond']
  spec.email         = ['vincentrobbemond@gmail.com']
  spec.homepage      = 'https://gitlab.com/maas-code/newsriver-api'
  spec.summary       = 'A Ruby client to communicate with the Newsriver API'
  spec.licenses      = ['Nonstandard']

  spec.files         = Dir['LICENSE.md', 'README.md', 'lib/**/*']
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0.1'

  spec.add_runtime_dependency 'oj', '~> 3.8'
end

